import sys
import os
import math

def cclear():
    os.system("cls||clear")

def calculator():
    
    def add(x,y):
        return x + y
    def subtract(x,y):
        return x - y
    def multiply(x,y):
        return x * y
    def divide(x,y):
        return x / y
    
    print("Select operation:\n")
    print("1) Add")
    print("2) Subtract")
    print("3) Multiply")
    print("4) Divide")
    print("5) Square root")
    print("6) Go back to main menu\n")

    choice = input("Enter choice (1-6): ")

    if choice == "5":
        num = int(input("Number: "))
        cclear()
        print("Result from operation: Sqrt (", num, ") =", math.sqrt(num), "\n")
        calculator()

    elif choice == "6":
        cclear()
        init()

    n1 = int(input("First number: "))
    n2 = int(input("Second number: "))

    if choice == "1":
        cclear()
        print("Result from operation:", n1, "+", n2, "=", add(n1,n2), "\n")
        calculator()

    elif choice == "2":
        cclear()
        print("Result from operation:", n1, "-", n2, "=", subtract(n1,n2), "\n")
        calculator()

    elif choice == "3":
        cclear()
        print("Result from operation:", n1, "*", n2, "=", multiply(n1,n2), "\n")
        calculator()

    elif choice == "4":
        cclear()
        print("Result from operation:", n1, "/", n2, "=", divide(n1,n2), "\n")
        calculator()

    else:
        cclear()
        print("Invalid operation!\n")
        calculator()

def counter():
    print("Select choice:\n")
    print("1) Enter counter mode")
    print("2) Go back to main menu\n")
    choice = input("Enter choice (1/2): ")

    if choice == "1":
        cclear()
        num = int(input("List numbers to: "))
        i = 0
        while i <= num:
            print(i)
            i += 1
        print("")
        counter()
    elif choice == "2":
        cclear()
        init()
    else:
        cclear()
        print("Invalid choice!\n")
        counter()

def init():
    print("Select applet:\n")
    print("1) Calculator")
    print("2) Counter")
    print("3) Exit\n")

    choice = input("Enter choice (1-3): ")

    if choice == "1":
        cclear()
        calculator()

    elif choice == "2":
        cclear()
        counter()

    elif choice == "3":
        sys.exit()

    else:
        print("Invalid choice!")

init()
